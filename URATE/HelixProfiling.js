function changeView() {
  var gridIcons = document.getElementsByName("gridIcon");
  var listIcons = document.getElementsByName("listIcon");

  var profileStyle = document.getElementsByName("profile");
  var tagStyle = document.getElementsByName("tag-wrapper");

  if (window.getComputedStyle(listIcons[0], null).getPropertyValue("display") === "none") {
    listIcons[0].style.display = '';
    listIcons[1].style.display = 'none';
    gridIcon[0].style.display = 'none';
    gridIcon[1].style.display = '';

    for (var i = 0; i < profileStyle.length; i++) {
      profileStyle[i].className = 'listProfile';
    }

    for (var i = 0; i < tagStyle.length; i++) {
      tagStyle[i].className = 'listProfileTagWrapper';
    }
  } else {
    listIcons[0].style.display = 'none';
    listIcons[1].style.display = '';
    gridIcon[0].style.display = '';
    gridIcon[1].style.display = 'none';

    for (var i = 0; i < profileStyle.length; i++) {
      profileStyle[i].className = 'gridProfile';
    }

    for (var i = 0; i < tagStyle.length; i++) {
      tagStyle[i].className = 'gridProfileTagWrapper';
    }
  }

  if (profileStyle.length % 3 === 1) {
    profileStyle[length - 1].style.visibility = 'hidden';
  } else if (profileStyle.length % 3 === 2) {
    profileStyle[length - 2].style.visibility = 'hidden';
    profileStyle[length - 1].style.visibility = 'hidden';
  }
}
function clickExport() {
  var exportNowButton = document.getElementById("exportNowButton");
  if (window.getComputedStyle(exportNowButton, null).getPropertyValue("display") === "none") {
    exportNowButton.style.display = '';
    var exportButton = document.getElementById("exportButton");
    exportButton.style.display = 'none';

    var checkboxes = document.getElementsByName("export");
    var options = document.getElementsByName("options");

    console.log('checkboxes', checkboxes, 'options', options);

    for (var i = 0; i < checkboxes.length; i++) {
      console.log('i: ', i);
      checkboxes[i].style.display = '';
      options[i].style.display = 'none';
    }
  } else {
    exportNowButton.style.display = 'none';
    var exportButton = document.getElementById("exportButton");
    exportButton.style.display = '';

    var checkboxes = document.getElementsByName("export");
    var options = document.getElementsByName("options");

    console.log('checkboxes', checkboxes, 'options', options);

    for (var i = 0; i < checkboxes.length; i++) {
      console.log('i: ', i);
      checkboxes[i].style.display = 'none';
      options[i].style.display = '';
    }
  }
}
<div id="tree">
  <ul>
    <li>Media
      <ul>
        <li>Cinema
          <ul>
            <li>Cinema Attendance
            <ul>
              <li>Cinema Last 12 Months</li>
              <ul>
                <li>Been to Cinema (last 12 months)</li>
                <li>Not Been (last 12 months)</li>
              </ul>
              <li>Cinema - Last 3 Months</li>
              <li>Cinema - Last 4 Weeks</li>    
            </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
</div>

  <script>
    var instantiateTree = function(name) {
      $("#" + name + " #tree").jstree({
          "checkbox": {
              "keep_selected_style": false
          },
          "plugins": ["checkbox"],
          "core": { 
            "themes":{
              "icons":false
            }
          },
      });
      $("#" + name + " #tree").jstree("open_all");

      $("#" + name + " #tree").bind("changed.jstree",
      function (e, data) {
        $("#" + name + " .tagsProfile ul").empty();
        var selected = $("#" + name + " #tree").jstree(true).get_selected("full", true);
        var leaf = selected.filter(function(node) {
          return node.children.length == 0;
        });

        leaf.forEach((el) => {
          var text = el.text;
          $("#" + name + " .tagsProfile ul").append("<li>" + text + "</li>");
        });
      });
    }

    $(function () {
      instantiateTree("editModal");
      instantiateTree("createModal");
    });
    

  </script>
<!-- </body>

</html> -->
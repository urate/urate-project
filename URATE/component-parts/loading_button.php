<div class="bootstrap-iso">
  <div class="row">
    <div class="col-md-3 col-md-offset-3">
      <button id="loadingButton" class="btn rounded red-text">Process</button>
      <div class="loader"></div>
      <script>
        $(".loader").hide();
      </script>
    </div>
  </div>
</div>

<script>
   $(function () {
      $("#table_id tbody").hide();    
      $(".bootstrap-iso #loadingButton").click(function() {
        $(".bootstrap-iso #loadingButton").hide();
        $(".loader").show();
        setTimeout(function(){ 
          $(".loader").hide();
          $(".bootstrap-iso #loadingButton").show();
          $(".bootstrap-iso #loadingButton").text("Done");
          $("#table_id tbody").show()
        }, 2000);
      })

   })
</script>
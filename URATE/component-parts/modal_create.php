<div class="modal fade" id="createModal" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create profile</h4>
      </div>
      <div class="modal-body">
        <input type="text" class="form-control rounded-input" />
        <h4>Group</h4>
        <div class="tagsProfile">
          <ul class="list-inline">
          </ul>
        </div>

        <h4>Parameter</h4>
        <div class="group">
          <?php require "group_profile.php";?>
        </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal" id="createProfileButton">Save</button>
      </div>
    </div>
    
  </div>
</div>

  <script>
    $("#createProfileButton").click(() => alert("Profile created"));
  </script>
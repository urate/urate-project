  <!-- Modal -->
  <div class="modal fade" id="deleteModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete</h4>
        </div>
        <div class="modal-body">
          Are you sure you want to delete this profile?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn red-text rounded" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger rounded" data-dismiss="modal" id="deleteButton">Delete</button>
        </div>
      </div>
      
    </div>
  </div>

  <script>
    $("#deleteButton").click(() => alert("Profile deleted"));
  </script>